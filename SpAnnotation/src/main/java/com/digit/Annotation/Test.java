package com.digit.Annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		
		//ApplicationContext is used to manage the object instead of directly creating objects
		//ClassPathXmlApplicationContext is a way to load the classPath to the Test class
		ApplicationContext context= new ClassPathXmlApplicationContext("beans.xml");
		
		//creating object of Eat class and fetching properties of eat class from bean.xml
		Office c=(Office) context.getBean("office");
		System.out.print(c.getPr());
		c.showInfo();
	}

}
