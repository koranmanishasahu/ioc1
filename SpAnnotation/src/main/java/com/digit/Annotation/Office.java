package com.digit.Annotation;

import org.springframework.beans.factory.annotation.Autowired;

public class Office {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Employee getPr() {
		return pr;
	}

	public void setPr(Employee pr) {
		this.pr = pr;
	}
//	public College(String name, Principal pr) {
//		super();
//		this.name = name;
//		this.pr = pr;
//	}
	@Autowired
	private Employee pr;
	
	public void showInfo()
	{
		System.out.println("Works in a nice company");
	}

}
