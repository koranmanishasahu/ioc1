package com.digit.demoBean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;



@Configuration   //configure own java class
  
@ComponentScan(basePackages="com.digit.demoBean") //Scans the bean to be injected

public class Config {
    @Bean
    public void showCollegeInfo() {
        System.out.println("MIT");
        
    }
}