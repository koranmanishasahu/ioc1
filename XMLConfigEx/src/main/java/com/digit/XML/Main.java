//declared package name
package com.digit.XML;
//importing necessary libraries
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main {

	public static void main(String[] args) {
		
		//ApplicationContext is used to manage the object instead of directly creating objects
		//ClassPathXmlApplicationContext is a way to load the classPath to the main class
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		
		//creating object of Eat class and fetching properties of eat class from bean.xml
	      Eat e = (Eat) context.getBean("eat");
	      System.out.println("Fav vegetable "+e.getVegetables());
	      System.out.println("color of apple is "+e.getFruit().getApple());
	      System.out.println("color of banana is "+e.getFruit().getBanana());

	}

}
